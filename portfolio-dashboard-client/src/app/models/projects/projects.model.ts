export interface IProject {
    _id?: string;
    __v?: number;
    title: string;
    subtitle?: string;
    imageUri?: string;
    description?: string;
}