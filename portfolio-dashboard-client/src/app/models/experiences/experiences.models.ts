import { Moment } from "moment";

export interface IExperience {
    _id?: string;
    __v?: number;
    title: string;
    subtitle?: string;
    startDate?: Moment;
    endDate?: Moment;
    description?: string;
}