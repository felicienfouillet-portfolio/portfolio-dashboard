import { Moment } from "moment";

export interface ITraining {
    _id?: string;
    __v?: number;
    title: string;
    subtitle?: string;
    startDate?: Moment;
    endDate?: Moment;
    description?: string;
}