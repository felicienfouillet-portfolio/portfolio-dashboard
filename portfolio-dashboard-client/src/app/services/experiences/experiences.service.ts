import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractApiService, NotificationsService } from '@felicienfouillet/ngx-csf-core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ExperiencesService extends AbstractApiService {
  constructor(private readonly http: HttpClient, private readonly _notificationService: NotificationsService) {
    super(http, _notificationService);
    this.endpoint = environment.apis.baseURL;
  }
}
