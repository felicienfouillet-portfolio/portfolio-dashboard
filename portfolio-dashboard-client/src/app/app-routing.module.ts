import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthMiddleware } from '@felicienfouillet/ngx-csf-core';
import { ExperiencesComponent } from './components/experiences/experiences.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { TrainingsComponent } from './components/trainings/trainings.component';

const routes: Routes = [
  // {
  //   path: 'blog',
  //   component: BlogComponent
  // },
  {
    path: 'projects',
    canActivate: [AuthMiddleware],
    component: ProjectsComponent
  },
  {
    path: 'trainings',
    canActivate: [AuthMiddleware],
    component: TrainingsComponent
  },
  {
    path: 'experiences',
    canActivate: [AuthMiddleware],
    component: ExperiencesComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: HomeComponent
  },
  {
    path: '**',
    redirectTo: '/',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
