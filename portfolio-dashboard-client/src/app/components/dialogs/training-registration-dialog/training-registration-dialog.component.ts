import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ITraining } from 'src/app/models/trainings/trainings.models';

import { Moment } from 'moment';

@Component({
  selector: 'app-training-registration-dialog',
  templateUrl: './training-registration-dialog.component.html',
  styleUrls: ['./training-registration-dialog.component.scss']
})
export class TrainingRegistrationDialogComponent implements OnInit {
  public isValidationDisabled: boolean = true;

  private _startDate: Moment;
  get startDate(): Moment {
    return this._startDate;
  }
  set startDate(value: Moment) {
    this._startDate = value;
    this.training.startDate = value;
  }

  private _endDate: Moment;
  get endDate(): Moment {
    return this._endDate;
  }
  set endDate(value: Moment) {
    this._endDate = value;
    this.training.endDate = value;
  }

  public training: ITraining = {
    title: '',
    subtitle: '',
    startDate: null,
    endDate: null,
    description: ''
  };

  constructor(public dialogRef: MatDialogRef<TrainingRegistrationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private readonly adapter: DateAdapter<any>) {
    this.adapter.setLocale('fr');
  }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ newTraining: this.training });
  }

  public toggleValidation(): void {
    if (!this.training.title.length) {
      this.isValidationDisabled = true;
      return;
    }

    this.isValidationDisabled = false;
  }
}
