import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingRegistrationDialogComponent } from './training-registration-dialog.component';

describe('TrainingRegistrationDialogComponent', () => {
  let component: TrainingRegistrationDialogComponent;
  let fixture: ComponentFixture<TrainingRegistrationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingRegistrationDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrainingRegistrationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
