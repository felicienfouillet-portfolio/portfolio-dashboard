import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectModificationDialogComponent } from './project-modification-dialog.component';

describe('ProjectModificationDialogComponent', () => {
  let component: ProjectModificationDialogComponent;
  let fixture: ComponentFixture<ProjectModificationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectModificationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectModificationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
