import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IProject } from 'src/app/models/projects/projects.model';

@Component({
  selector: 'app-project-modification-dialog',
  templateUrl: './project-modification-dialog.component.html',
  styleUrls: ['./project-modification-dialog.component.scss']
})
export class ProjectModificationDialogComponent implements OnInit {
  public isValidationDisabled: boolean = true;

  public project: IProject = {
    title: '',
    subtitle: '',
    description: '',
    imageUri: ''
  };

  constructor(public dialogRef: MatDialogRef<ProjectModificationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.project = data.project;
    
    delete this.project._id;
    delete this.project.__v;
  }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ updatedProject: this.project });
  }

  public toggleValidation(): void {
    if (!this.project.title.length) {
      this.isValidationDisabled = true;
      return;
    }
    
    this.isValidationDisabled = false;
  }
}
