import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceModificationDialogComponent } from './experience-modification-dialog.component';

describe('ExperienceModificationDialogComponent', () => {
  let component: ExperienceModificationDialogComponent;
  let fixture: ComponentFixture<ExperienceModificationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperienceModificationDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExperienceModificationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
