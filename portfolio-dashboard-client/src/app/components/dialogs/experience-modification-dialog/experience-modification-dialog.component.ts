import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { Moment } from 'moment';
import { IExperience } from 'src/app/models/experiences/experiences.models';

@Component({
  selector: 'app-experience-modification-dialog',
  templateUrl: './experience-modification-dialog.component.html',
  styleUrls: ['./experience-modification-dialog.component.scss']
})
export class ExperienceModificationDialogComponent implements OnInit {

  public isValidationDisabled: boolean = true;

  private _startDate: Moment;
  get startDate(): Moment {
    return this._startDate;
  }
  set startDate(value: Moment) {
    this._startDate = value;
    this.experience.startDate = value;
  }

  private _endDate: Moment;
  get endDate(): Moment {
    return this._endDate;
  }
  set endDate(value: Moment) {
    this._endDate = value;
    this.experience.endDate = value;
  }

  public experience: IExperience = {
    title: '',
    subtitle: '',
    startDate: null,
    endDate: null,
    description: ''
  };

  constructor(public dialogRef: MatDialogRef<ExperienceModificationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private readonly adapter: DateAdapter<any>) {
    this.adapter.setLocale('fr');

    this.experience = data.experience;

    this.startDate = moment(this.experience.startDate);
    this.endDate = moment(this.experience.endDate);

    delete this.experience._id;
    delete this.experience.__v;
  }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ updatedExperience: this.experience });
  }

  public toggleValidation(): void {
    if (!this.experience.title.length) {
      this.isValidationDisabled = true;
      return;
    }

    this.isValidationDisabled = false;
  }
}
