import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingsModificationDialogComponent } from './trainings-modification-dialog.component';

describe('TrainingsModificationDialogComponent', () => {
  let component: TrainingsModificationDialogComponent;
  let fixture: ComponentFixture<TrainingsModificationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingsModificationDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrainingsModificationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
