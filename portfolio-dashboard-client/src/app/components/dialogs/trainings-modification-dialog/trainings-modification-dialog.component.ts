import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import * as moment from 'moment';
import { Moment } from 'moment';
import { ITraining } from 'src/app/models/trainings/trainings.models';

@Component({
  selector: 'app-trainings-modification-dialog',
  templateUrl: './trainings-modification-dialog.component.html',
  styleUrls: ['./trainings-modification-dialog.component.scss']
})
export class TrainingsModificationDialogComponent implements OnInit {

  public isValidationDisabled: boolean = true;

  private _startDate: Moment;
  get startDate(): Moment {
    return this._startDate;
  }
  set startDate(value: Moment) {
    this._startDate = value;
    this.training.startDate = value;
  }

  private _endDate: Moment;
  get endDate(): Moment {
    return this._endDate;
  }
  set endDate(value: Moment) {
    this._endDate = value;
    this.training.endDate = value;
  }

  public training: ITraining = {
    title: '',
    subtitle: '',
    startDate: null,
    endDate: null,
    description: ''
  };

  constructor(public dialogRef: MatDialogRef<TrainingsModificationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private readonly adapter: DateAdapter<any>) {
    this.adapter.setLocale('fr');

    this.training = data.training;

    this.startDate = moment(this.training.startDate);
    this.endDate = moment(this.training.endDate);

    delete this.training._id;
    delete this.training.__v;
  }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ updatedTraining: this.training });
  }

  public toggleValidation(): void {
    if (!this.training.title.length) {
      this.isValidationDisabled = true;
      return;
    }

    this.isValidationDisabled = false;
  }
}
