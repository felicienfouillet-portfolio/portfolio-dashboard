import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectRegistrationDialogComponent } from './project-registration-dialog.component';

describe('ProjectRegistrationDialogComponent', () => {
  let component: ProjectRegistrationDialogComponent;
  let fixture: ComponentFixture<ProjectRegistrationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProjectRegistrationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectRegistrationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
