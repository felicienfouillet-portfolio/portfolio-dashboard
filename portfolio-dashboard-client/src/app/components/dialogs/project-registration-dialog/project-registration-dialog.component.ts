import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IProject } from 'src/app/models/projects/projects.model';

@Component({
  selector: 'app-project-registration-dialog',
  templateUrl: './project-registration-dialog.component.html',
  styleUrls: ['./project-registration-dialog.component.scss']
})
export class ProjectRegistrationDialogComponent implements OnInit {
  public isValidationDisabled: boolean = true;

  public project: IProject = {
    title: '',
    subtitle: '',
    description: '',
    imageUri: ''
  };

  constructor(public dialogRef: MatDialogRef<ProjectRegistrationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ newProject: this.project });
  }

  public toggleValidation(): void {
    if (!this.project.title.length) {
      this.isValidationDisabled = true;
      return;
    }

    this.isValidationDisabled = false;
  }
}
