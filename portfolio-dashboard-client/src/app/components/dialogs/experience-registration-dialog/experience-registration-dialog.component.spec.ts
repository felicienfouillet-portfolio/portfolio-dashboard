import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceRegistrationDialogComponent } from './experience-registration-dialog.component';

describe('ExperienceRegistrationDialogComponent', () => {
  let component: ExperienceRegistrationDialogComponent;
  let fixture: ComponentFixture<ExperienceRegistrationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperienceRegistrationDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceRegistrationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
