import { Component, Inject, OnInit } from '@angular/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Moment } from 'moment';
import { IExperience } from 'src/app/models/experiences/experiences.models';

@Component({
  selector: 'app-experience-registration-dialog',
  templateUrl: './experience-registration-dialog.component.html',
  styleUrls: ['./experience-registration-dialog.component.scss']
})
export class ExperienceRegistrationDialogComponent implements OnInit {
  public isValidationDisabled: boolean = true;

  private _startDate: Moment;
  get startDate(): Moment {
    return this._startDate;
  }
  set startDate(value: Moment) {
    this._startDate = value;
    this.experience.startDate = value;
  }

  private _endDate: Moment;
  get endDate(): Moment {
    return this._endDate;
  }
  set endDate(value: Moment) {
    this._endDate = value;
    this.experience.endDate = value;
  }

  public experience: IExperience = {
    title: '',
    subtitle: '',
    startDate: null,
    endDate: null,
    description: ''
  };

  constructor(public dialogRef: MatDialogRef<ExperienceRegistrationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, private readonly adapter: DateAdapter<any>) {
    this.adapter.setLocale('fr');
  }

  ngOnInit(): void {
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onCancel(): void {
    this.dialogRef.close();
  }

  public onValidate(): void {
    this.dialogRef.close({ newExperience: this.experience });
  }

  public toggleValidation(): void {
    if (!this.experience.title.length) {
      this.isValidationDisabled = true;
      return;
    }

    this.isValidationDisabled = false;
  }
}
