import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { Subject, take } from 'rxjs';
import { IExperience } from 'src/app/models/experiences/experiences.models';
import { ExperiencesService } from 'src/app/services/experiences/experiences.service';
import { ExperienceModificationDialogComponent } from '../dialogs/experience-modification-dialog/experience-modification-dialog.component';
import { ExperienceRegistrationDialogComponent } from '../dialogs/experience-registration-dialog/experience-registration-dialog.component';

@Component({
  selector: 'app-experiences',
  templateUrl: './experiences.component.html',
  styleUrls: ['./experiences.component.scss']
})
export class ExperiencesComponent implements OnInit, OnDestroy {

  public experiences: Array<IExperience> = new Array<IExperience>();

  get isMobile(): boolean {
    if (this._settingsService.currentBreakPoint === '(max-width: 599.98px)') return true;
    return false;
  }

  private readonly onDestroy$: Subject<void> = new Subject<void>();

  constructor(private readonly dialog: MatDialog, private readonly _experiencesService: ExperiencesService, private readonly _settingsService: SettingsService) { }

  ngOnInit(): void {
    this.updateExperienceList();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  registerNewExperience(): void {
    this.dialog.open(ExperienceRegistrationDialogComponent).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.newExperience) {
        this._experiencesService.post({
          apiUrl: '/experiences',
          body: result.newExperience
        }).pipe(take(1)).subscribe(() => {
          this.updateExperienceList();
        });
      }
    });
  }

  deleteExperience(event: Event, experienceId: string): void {
    event.stopPropagation();

    this._experiencesService.delete({
      apiUrl: '/experiences',
      params: {
        id: experienceId
      }
    }).pipe(take(1)).subscribe(() => {
      this.updateExperienceList();
    });
  }

  updateExperience(event: Event, experienceId: string): void {
    event.stopPropagation();

    this.dialog.open(ExperienceModificationDialogComponent, { data: { experience: this.experiences.find(experience => experience._id === experienceId) } }).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.updatedExperience) {
        this._experiencesService.patch({
          apiUrl: '/experiences',
          params: {
            id: experienceId
          },
          body: result.updatedExperience
        }).pipe(take(1)).subscribe(result => {
          this.updateExperienceList();
        });
      }
    });
  }

  updateExperienceList(): void {
    this._experiencesService.get({
      apiUrl: '/experiences'
    }).pipe(take(1)).subscribe((result: any) => {
      this.experiences = result;
    });
  }
}
