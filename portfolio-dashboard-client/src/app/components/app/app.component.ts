import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DEFAULT_USERS_API_OPTIONS, SettingsService, UserService } from '@felicienfouillet/ngx-csf-core';
import { Subject, take } from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  public isLoading: boolean = false;

  public get isUserConnected(): boolean {
    return this._userService.isUserConnected;
  };

  get isMobile(): boolean {
    return this._settingsService.mediaScreenInfos.isMobile;
  }

  get isDarkMode(): boolean {
    return this._settingsService.getProperty('general-isDarkMode') as boolean;
  }
  set isDarkMode(isDarkMode: boolean) {
    this._settingsService.setProperty('general-isDarkMode', isDarkMode);
    this._settingsService.saveStore();
    this._settingsService.changeTheme(isDarkMode);
  }

  private readonly onDestroy$: Subject<void> = new Subject<void>();

  constructor(
    private readonly router: Router,
    private readonly _settingsService: SettingsService,
    private readonly _userService: UserService
  ) {
    this.isLoading = true;
  }

  ngOnInit(): void {
    this._userService.API_OPTIONS = {
      ...DEFAULT_USERS_API_OPTIONS,
      ENDPOINT: environment.apis.users.baseURL
    };

    this._userService.initService().pipe(take(1)).subscribe(() => {
      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  logout(): void {
    this.isLoading = true;
    this._userService.logout({ mail: this._userService.connectedUser.mail }).pipe(take(1)).subscribe(() => {
      this.isLoading = false;
      this.router.navigate(['/']);
    });
  }
}
