import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { Subject, take } from 'rxjs';
import { IProject } from 'src/app/models/projects/projects.model';
import { ProjectsService } from 'src/app/services/projects/projects.service';
import { ProjectModificationDialogComponent } from '../dialogs/project-modification-dialog/project-modification-dialog.component';
import { ProjectRegistrationDialogComponent } from '../dialogs/project-registration-dialog/project-registration-dialog.component';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit, OnDestroy {

  public projects: Array<IProject> = new Array<IProject>();

  get isMobile(): boolean {
    if (this._settingsService.currentBreakPoint === '(max-width: 599.98px)') return true;
    return false;
  }

  private readonly onDestroy$: Subject<void> = new Subject<void>();

  constructor(private readonly dialog: MatDialog, private readonly _projectsService: ProjectsService, private readonly _settingsService: SettingsService) { }

  ngOnInit(): void {
    this.updateProjectList();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  registerNewProject(): void {
    this.dialog.open(ProjectRegistrationDialogComponent).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.newProject) {
        this._projectsService.post({
          apiUrl: '/projects',
          body: result.newProject
        }).pipe(take(1)).subscribe(result => {
          this.updateProjectList();
        });
      }
    });
  }

  deleteProject(event: Event, projectId: string): void {
    event.stopPropagation();

    this._projectsService.delete({
      apiUrl: '/projects',
      params: {
        id: projectId
      }
    }).pipe(take(1)).subscribe(result => {
      this.updateProjectList();
    });
  }

  updateProject(event: Event, projectId: string): void {
    event.stopPropagation();

    this.dialog.open(ProjectModificationDialogComponent, { data: { project: this.projects.find(project => project._id === projectId) } }).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.updatedProject) {
        this._projectsService.patch({
          apiUrl: '/projects',
          params: {
            id: projectId
          },
          body: result.updatedProject
        }).pipe(take(1)).subscribe(result => {
          this.updateProjectList();
        });
      }
    });
  }

  updateProjectList(): void {
    this._projectsService.get({
      apiUrl: '/projects'
    }).pipe(take(1)).subscribe((result: any) => {
      this.projects = result;
    });
  }
}
