import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SettingsService } from '@felicienfouillet/ngx-csf-core';
import { Subject } from 'rxjs';
import { take } from 'rxjs';
import { ITraining } from 'src/app/models/trainings/trainings.models';
import { TrainingsService } from 'src/app/services/trainings/trainings.service';
import { TrainingRegistrationDialogComponent } from '../dialogs/training-registration-dialog/training-registration-dialog.component';
import { TrainingsModificationDialogComponent } from '../dialogs/trainings-modification-dialog/trainings-modification-dialog.component';

@Component({
  selector: 'app-trainings',
  templateUrl: './trainings.component.html',
  styleUrls: ['./trainings.component.scss']
})
export class TrainingsComponent implements OnInit {

  public trainings: Array<ITraining> = new Array<ITraining>();

  get isMobile(): boolean {
    if (this._settingsService.currentBreakPoint === '(max-width: 599.98px)') return true;
    return false;
  }

  private readonly onDestroy$: Subject<void> = new Subject<void>();

  constructor(private readonly dialog: MatDialog, private readonly _trainingsService: TrainingsService, private readonly _settingsService: SettingsService) { }

  ngOnInit(): void {
    this.updateTrainingList();
  }

  ngOnDestroy(): void {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  registerNewTraining(): void {
    this.dialog.open(TrainingRegistrationDialogComponent).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.newTraining) {
        this._trainingsService.post({
          apiUrl: '/trainings',
          body: result.newTraining
        }).pipe(take(1)).subscribe(() => {
          this.updateTrainingList();
        });
      }
    });
  }

  deleteTraining(event: Event, trainingId: string): void {
    event.stopPropagation();

    this._trainingsService.delete({
      apiUrl: '/trainings',
      params: {
        id: trainingId
      }
    }).pipe(take(1)).subscribe(() => {
      this.updateTrainingList();
    });
  }

  updateTraining(event: Event, trainingId: string): void {
    event.stopPropagation();

    this.dialog.open(TrainingsModificationDialogComponent, { data: { training: this.trainings.find(training => training._id === trainingId) } }).afterClosed().pipe(take(1)).subscribe(result => {
      if (result?.updatedTraining) {
        this._trainingsService.patch({
          apiUrl: '/trainings',
          params: {
            id: trainingId
          },
          body: result.updatedTraining
        }).pipe(take(1)).subscribe(result => {
          this.updateTrainingList();
        });
      }
    });
  }

  updateTrainingList(): void {
    this._trainingsService.get({
      apiUrl: '/trainings'
    }).pipe(take(1)).subscribe((result: any) => {
      this.trainings = result;
    });
  }
}
