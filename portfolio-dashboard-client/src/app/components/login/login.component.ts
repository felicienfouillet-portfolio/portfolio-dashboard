import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService, UserService } from '@felicienfouillet/ngx-csf-core';
import { take } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public isValidationDisabled: boolean = true;

  public mail: string;
  public password: string;

  constructor(private readonly router: Router, private readonly _userService: UserService, private readonly _notificationsService: NotificationsService) { }

  ngOnInit(): void {
    if (this._userService.isUserConnected) {
      this.router.navigate(['']);
    }
    this.toggleValidation();
  }

  public onKeyPress(event?: KeyboardEvent): void {
    if (event && event.key === 'Enter') {
      this.onValidate();
    }
  }

  public onValidate(): void {
    this._userService.login({ mail: this.mail, password: this.password }).pipe(take(1)).subscribe(() => {
      if (this._userService.isUserConnected) {
        this.router.navigate(['/projects']);
        return;
      }
      this._notificationsService.notify({ type: 'error', message: 'Error during connection process' });
    });
  }

  public toggleValidation(): void {
    if (!this.password?.length || !this.mail?.length) {
      this.isValidationDisabled = true;
      return;
    }
    this.isValidationDisabled = false;
  }
}
