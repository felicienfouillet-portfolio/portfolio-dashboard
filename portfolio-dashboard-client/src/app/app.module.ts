import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxCsfModule } from '@felicienfouillet/ngx-csf-core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app/app.component';
import { ExperienceModificationDialogComponent } from './components/dialogs/experience-modification-dialog/experience-modification-dialog.component';
import { ExperienceRegistrationDialogComponent } from './components/dialogs/experience-registration-dialog/experience-registration-dialog.component';
import { ProjectModificationDialogComponent } from './components/dialogs/project-modification-dialog/project-modification-dialog.component';
import { ProjectRegistrationDialogComponent } from './components/dialogs/project-registration-dialog/project-registration-dialog.component';
import { TrainingRegistrationDialogComponent } from './components/dialogs/training-registration-dialog/training-registration-dialog.component';
import { TrainingsModificationDialogComponent } from './components/dialogs/trainings-modification-dialog/trainings-modification-dialog.component';
import { ExperiencesComponent } from './components/experiences/experiences.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { TrainingsComponent } from './components/trainings/trainings.component';
import { MaterialModule } from './modules/material/material.module';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProjectsComponent,
    ProjectRegistrationDialogComponent,
    ProjectModificationDialogComponent,
    ExperiencesComponent,
    ExperienceRegistrationDialogComponent,
    LoginComponent,
    TrainingsComponent,
    TrainingRegistrationDialogComponent,
    ExperienceModificationDialogComponent,
    TrainingsModificationDialogComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgxCsfModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
