export const environment = {
  production: true,
  apis: {
    baseURL: 'https://portfolio-back.felicien-fouillet.fr/api/v1',
    users: {
      baseURL: 'https://portfolio-auth.felicien-fouillet.fr/api/v1',
      endpoints: {
        login: '/auth/login',
        logout: '/auth/logout',
        verifyToken: '/auth/verifyToken',
        refreshToken: '/auth/refreshToken',
        register: '/auth/register'
      }
    }
  }
};
