export const environment = {
  production: false,
  apis: {
    baseURL: 'http://localhost:3030/api/v1',
    users: {
      baseURL: 'http://localhost:3031/api/v1',
      endpoints: {
        login: '/auth/login',
        logout: '/auth/logout',
        verifyToken: '/auth/verifyToken',
        refreshToken: '/auth/refreshToken',
        register: '/auth/register'
      }
    }
  }
};
